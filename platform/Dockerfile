# Build Stage Develop
FROM ubuntu:18.04

ARG PHP_VERSION="7.2"
ARG APP_ENV=develop

#Install Nginx

RUN apt-get update \
    && apt-get install -y --no-install-recommends software-properties-common \
    && apt-add-repository -y ppa:nginx/stable \
    && apt-get update \
    && apt-get install -y --no-install-recommends --no-install-suggests nginx \
    && rm -rf /var/lib/apt/lists/*

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log


ADD config/nginx.conf /etc/nginx/sites-available/default

# pgsql client

ARG INSTALL_PG_CLIENT=true
RUN if [ ${INSTALL_PG_CLIENT} = true ]; then \
    # Create folders if not exists (https://github.com/tianon/docker-brew-debian/issues/65)
    mkdir -p /usr/share/man/man1 && \
    mkdir -p /usr/share/man/man7 && \
    # Install the pgsql client
    apt-get update -yqq && \
    apt-get install -y --no-install-recommends postgresql-client && \
    rm -rf /var/lib/apt/lists/* \
;fi

#Install PHP

RUN apt-get update \
    && add-apt-repository -y ppa:ondrej/php \
    && apt-get update \
    && apt-get install -y --no-install-recommends php${PHP_VERSION} php${PHP_VERSION}-imagick php${PHP_VERSION}-mbstring php${PHP_VERSION}-common  php${PHP_VERSION}-cli php${PHP_VERSION}-gd php${PHP_VERSION}-curl php${PHP_VERSION}-xml php${PHP_VERSION}-json php${PHP_VERSION}-zip  php${PHP_VERSION}-fpm php${PHP_VERSION}-pgsql \
    && apt-get -y install curl \
    && rm -rf /var/lib/apt/lists/*

# Install composer and add its bin to the PATH.
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer
RUN composer self-update --1

RUN usermod -a -G root www-data

# Install Crontab
RUN apt-get update \
    && apt-get -y --no-install-recommends install cron \
    && rm -rf /var/lib/apt/lists/*


# Workspace
WORKDIR /var/www

EXPOSE 80
CMD ["/bin/bash", "-c", "/usr/sbin/service php7.2-fpm start && nginx -g 'daemon off;'"]
